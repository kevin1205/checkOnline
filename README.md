# 本程序用于统计博学谷直播平台在线人数


## 使用方法

1. 打开names.js， 将学生姓名复制粘贴过去（注意，学生姓名要和直播平台昵称保持一致）
2. 运行 app.js，输入讲师或助教直播地址即可

- 举例：
- node app.js "https://view.csslcloud.net/api/view/assistant?roomid=A60BC2653C035BE79C33DC5901307461&userid=44CACEB725BAB224"
- 输出：
    ![示例](示例.jpg)